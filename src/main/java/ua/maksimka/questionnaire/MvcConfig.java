package ua.maksimka.questionnaire;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/registration").setViewName("registration");
        registry.addViewController("/fields/fieldsView").setViewName("fieldsView");
        registry.addViewController("/").setViewName("questionnaire");
        registry.addViewController("/responses").setViewName("responses");
        registry.addViewController("/error").setViewName("error");
        registry.addViewController("/thanks").setViewName("thanks");

    }

}