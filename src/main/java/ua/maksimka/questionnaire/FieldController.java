package ua.maksimka.questionnaire;


import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.expression.spel.CodeFlow;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.maksimka.questionnaire.beans.FieldAdder;
import ua.maksimka.questionnaire.beans.FieldView;
import ua.maksimka.questionnaire.data.*;

import javax.faces.event.ActionEvent;

import java.util.*;


@Controller
@RequestMapping("/fields")
public class FieldController {

    @Autowired
    private FieldRepo repo;

    @Autowired
    private CheckBoxOptionsRepo checkboxRepo;

    @Autowired
    private ResponseRepo responseRepo;

    @Autowired
    private ResponseDataRepo responseDataRepo;

    @Autowired
    private FieldAdder adder;

    @Autowired
    private FieldView fieldView;

    @GetMapping("")
    public ModelAndView showFields(){
        ModelAndView model = new ModelAndView("fieldsView");
        return model;

    }


    @GetMapping("/{id}")
    public ModelAndView viewStudent(@PathVariable("id") long id) {
        Field field = repo.findById(id).get();
        FieldType it = field.getType();
        adder.setId(id);
        adder.setType(it.name());
        adder.setLabel(field.getLabel());
        adder.setFieldOptions(new String());
        List<CheckBoxOption> options = checkboxRepo.getAllByField(repo.findById(id).get());
        for(CheckBoxOption option : options) {
            adder.setFieldOptions(adder.getFieldOptions().concat(option.getOptionValue()));
            adder.setFieldOptions(adder.getFieldOptions().concat("\n"));
        }
        ModelAndView model = new ModelAndView("//updateField");
        //fieldView.addField();
        return model;
    }

    @GetMapping("/delete/{id}")
    public String deleteField(@PathVariable("id") long id){
        Field field = repo.findById(id).get();
        checkboxRepo.deleteAllByField(field);
        responseDataRepo.deleteAllByField(field);
        repo.delete(field);

        return "redirect:/fields";
    }

    @GetMapping("/qstresponse")
   public String qstresponse(@RequestParam HashMap<String, String> objects){
        Response response = new Response();
        for (Map.Entry<String, String> entry : objects.entrySet()) {
            if (entry.getKey().contains(".")) {   //if param is checkbox option
                StringTokenizer tokenizer = new StringTokenizer(entry.getKey(), ".");
                response.getResponseDataSet().add(new ResponseData((repo.findById(Long.parseLong(tokenizer.nextToken()))).get(), entry.getValue()));
            } else {
                response.getResponseDataSet().add(new ResponseData((repo.findById(Long.parseLong(entry.getKey()))).get(), entry.getValue()));
            }
        }
        responseRepo.save(response);

         return "redirect:/thanks";
    }


}
