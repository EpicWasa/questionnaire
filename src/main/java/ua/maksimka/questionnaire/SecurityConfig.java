package ua.maksimka.questionnaire;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // require all requests to be authenticated except for the resources
        http.authorizeRequests().antMatchers("/","/thanks", "/javax.faces.resource/**", "/registration.xhtml", "/fields/qstresponse")
                .permitAll().anyRequest().authenticated();
        // login
        http.formLogin().loginPage("/login.xhtml").permitAll().defaultSuccessUrl("/",true)
                .failureUrl("/login.xhtml?error=true");
        http.headers().frameOptions().sameOrigin();
        // logout
        http.logout().logoutSuccessUrl("/login.xhtml")
                .logoutUrl("/logout");
        // not needed as JSF 2.2 is implicitly protected against CSRF
        http.csrf().disable();
         ;

//        http
//                .authorizeRequests()
//                .antMatchers("/", "/home").permitAll()
//                .anyRequest().authenticated()
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .permitAll()
//                .and()
//                .logout()
//                .permitAll();

    }

//    @Autowired
////    public void configureGlobal(AuthenticationManagerBuilder auth)
////            throws Exception {
////        auth.inMemoryAuthentication().withUser("1")
////                .password("{noop}1").roles("USER").and()
////                .withUser("jane.doe").password("{noop}5678").roles("ADMIN");
////    }
////
////    @Bean
////    @Override
////    public UserDetailsService userDetailsService() {
////        UserDetails user =
////                User.withDefaultPasswordEncoder()
////                        .username("u")
////                        .password("p")
////                        .roles("USER")
////                        .build();
////
////        return new InMemoryUserDetailsManager(user);
////    }


    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(NoOpPasswordEncoder.getInstance())
                .usersByUsernameQuery("select usr.email , usr.password , usr.active from usr where usr.email = ? ")
                .authoritiesByUsernameQuery("select usr.email, usr.roles from usr where usr.email = ?");
    }
}