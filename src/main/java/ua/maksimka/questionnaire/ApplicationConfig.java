package ua.maksimka.questionnaire;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
// Указываем, относительно какого пакета искать компоненты
public class ApplicationConfig {

    // ViewResolver - указывает где и каким будет view
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver =
                new InternalResourceViewResolver();
        viewResolver.setPrefix("");
        viewResolver.setSuffix(".jsf");
        return viewResolver;
    }
}
