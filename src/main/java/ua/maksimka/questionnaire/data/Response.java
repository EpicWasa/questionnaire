package ua.maksimka.questionnaire.data;


import lombok.Getter;
import lombok.Setter;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Response {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "data_id")
    Set<ResponseData> responseDataSet = new HashSet<>();

    public Set<ResponseData> getResponseDataSet() {
        return responseDataSet;
    }

    public void setResponseDataSet(Set<ResponseData> responseDataSet) {
        this.responseDataSet = responseDataSet;
    }
}
