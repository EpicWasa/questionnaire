package ua.maksimka.questionnaire.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ResponseDataRepo extends CrudRepository<ResponseData, Long> {
    //public ResponseData findByFieldNameAnd;

    @Transactional
    public void deleteAllByField(Field field);


}
