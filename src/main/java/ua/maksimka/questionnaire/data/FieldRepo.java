package ua.maksimka.questionnaire.data;

import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface FieldRepo extends CrudRepository<Field, Long> {
    public List<Field> findAllByActive(boolean active);


}
