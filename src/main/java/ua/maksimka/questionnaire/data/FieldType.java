package ua.maksimka.questionnaire.data;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.awt.*;



public enum FieldType {
    TEXTFIELD,
    TEXTAREA,
    RADIOBUTTON,
    CHECKBOX,
    COMBOBOX,
    DATE;




}
