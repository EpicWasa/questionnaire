package ua.maksimka.questionnaire.data;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "usr")
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public User(String email, String firstName, String lastName, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    private String firstName;

    private String phone;

    private String lastName;

    private String email;

    private String password;

    private boolean active;

    public String getPhone() {
        return phone;
    }
}
