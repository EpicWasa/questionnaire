package ua.maksimka.questionnaire.data;

import lombok.Getter;
import lombok.Setter;

import javax.annotation.processing.Generated;
import javax.persistence.*;

@Getter
@Setter
@Entity
public class ResponseData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn( name = "field_id")
    private Field field;

    private String fieldValue;

    public ResponseData(Field field, String fieldValue) {
        this.field = field;
        this.fieldValue = fieldValue;
    }

    public ResponseData() {
    }
}
