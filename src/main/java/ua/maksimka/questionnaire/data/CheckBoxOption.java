package ua.maksimka.questionnaire.data;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class CheckBoxOption {

    public CheckBoxOption() {
    }

    public CheckBoxOption(String optionValue) {
        this.optionValue = optionValue;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn( name = "field_id")
    private Field field;

    private String optionValue;
}
