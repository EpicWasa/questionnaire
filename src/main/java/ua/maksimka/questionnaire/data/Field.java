package ua.maksimka.questionnaire.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor

public class Field {

    public Field(String name, FieldType type, boolean isActive, boolean isNeeded ) {
        this.label = name;
        this.active = isActive;
        this.required = isNeeded;
        this.type = type;

    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String label;

    private boolean active;
    private boolean required;



    @Enumerated(EnumType.STRING)
    private FieldType type;
}
