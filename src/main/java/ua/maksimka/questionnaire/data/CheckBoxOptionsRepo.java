package ua.maksimka.questionnaire.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface CheckBoxOptionsRepo extends CrudRepository<CheckBoxOption, Long> {
//       public CheckBoxOption getByField(long fieldId);
       public List<CheckBoxOption> getAllByField(Field field);

       @Transactional
       public void deleteAllByField(Field field);
}
