package ua.maksimka.questionnaire.data;

import org.springframework.data.repository.CrudRepository;

public interface ResponseRepo extends CrudRepository<Response, Long> {
}
