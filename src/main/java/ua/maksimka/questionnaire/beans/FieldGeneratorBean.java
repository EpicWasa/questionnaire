package ua.maksimka.questionnaire.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.maksimka.questionnaire.data.*;

import java.util.List;

@Getter
@Setter
@Component
public class FieldGeneratorBean {

    @Autowired
    private FieldRepo fieldRepo;

    @Autowired
    private CheckBoxOptionsRepo boxRepo;

    private String generate;

    public String generateInputFields(){

        String res = "";

        List<Field> fields =  fieldRepo.findAllByActive(true);
        for(Field field : fields){
            res = res.concat("<h3>"+field.getLabel()+"</h3>"+"\n");
          if(field.getType().equals(FieldType.TEXTFIELD)){

                    res = res.concat("<input name = '"+field.getId()+"' type = 'text' "+ required(field))+"/>"+"\n";
            }
            else if (field.getType().equals(FieldType.TEXTAREA)){
              res = res.concat("<textarea rows = '3' name = '"+field.getId()+"' cols = '20' class = 'ui-inputfield ui-inputtextarea ui-widget ui-state-default ui-corner-all ui-inputtextarea-resizable' "+ required(field)+"></textarea>"+"\n");
          }

            else if (field.getType().equals(FieldType.RADIOBUTTON)){
                for(CheckBoxOption option : boxRepo.getAllByField(field)){
                 res = res.concat("<div>\n");
                 res = res.concat("<input type=\"radio\" "+ required(field)+" id = '"+field.getId()+"' name=\""+field.getId()+"\" value=\""+option.getOptionValue()+"\"\n" +
                         "         checked>");
                 res = res.concat("<label for=\""+field.getId()+"\">"+option.getOptionValue()+"</label>\n" +
                         "</div>\n");
                }
          }

          else if (field.getType().equals(FieldType.CHECKBOX)){
              for(CheckBoxOption option : boxRepo.getAllByField(field)){
                  res = res.concat("<div>\n");
                  res = res.concat("<input type=\"checkbox\" id = '"+field.getId()+"' name=\""+field.getId()+"."+option.getOptionValue()+"\" value=\""+option.getOptionValue()+"\"\n" +
                          "         checked>");
                  res = res.concat("<label for=\""+field.getId()+"\">"+option.getOptionValue()+"</label>\n" +
                          "</div>\n");
              }
          }

          else if (field.getType().equals(FieldType.COMBOBOX)){
              res = res.concat("<select name = '"+field.getId()+"'"+ required(field)+">\n");
              for(CheckBoxOption option : boxRepo.getAllByField(field)){

                  res = res.concat("<option name = '' value=\""+option.getOptionValue()+"\"\n>"+option.getOptionValue()+"</option>\n");

              }
              res = res.concat("</select>\n");
          }

          else if (field.getType().equals(FieldType.DATE)){
              res = res.concat("<span class='p-datepicker ui-calendar' >\n");


                  res = res.concat("<input "+ required(field)+" name=\""+field.getId()+"\" class=\"ui-inputfield ui-widget ui-state-default ui-corner-all\" id=\"form:popup_input\" role=\"date\" aria-disabled=\"false\" aria-readonly=\"false\" aria-labelledby=\"form:j_idt724\" type=\"date\" data-p-label=\"Popup\">\n");


              res = res.concat("</span>\n");
          }

        }
        return res;
    }

    private String required(Field field){
        if(field.isRequired()){
            return "required";
        }
        else return  "";

    }

}
