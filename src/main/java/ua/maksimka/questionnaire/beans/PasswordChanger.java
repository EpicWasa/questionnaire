package ua.maksimka.questionnaire.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ua.maksimka.questionnaire.data.User;
import ua.maksimka.questionnaire.data.UserRepo;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@Getter
@Setter
@Component
public class PasswordChanger {

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    UserRepo repo;

    private String getCurrentUserPassword(){
        return repo.findByEmail(getCurrentUserEmail()).getPassword();


    }

    private String getCurrentUserEmail(){

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username = "";

        if (principal instanceof UserDetails) {

            username = ((UserDetails)principal).getUsername();

        } else {

            username = principal.toString();

        }

        return username;
    }

    private String currentPassword;
    private String newPassword;
    private String repeatedPassword;

    public void changePassword() throws Exception{
     if(!(currentPassword.equals(getCurrentUserPassword()))) {
         currentPassword = null;
         return;}
     if(newPassword!=null && !newPassword.equals(" ") && newPassword.equals(repeatedPassword)){
        User user =  repo.findByEmail(getCurrentUserEmail());
        user.setPassword(newPassword);
        currentPassword = null;
        newPassword = null;
        repeatedPassword = null;
        repo.save(user);
        //emailService.sendSimpleMessage(getCurrentUserEmail(), "Password Update", "Congrats!");
        setEmpty();
         ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext() ;
         externalContext.redirect("/");
     }
    }


    private void setEmpty(){
        currentPassword=null;
        newPassword=null;
        repeatedPassword=null;
    }
}
