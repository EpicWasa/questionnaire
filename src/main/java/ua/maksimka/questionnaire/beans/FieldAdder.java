package ua.maksimka.questionnaire.beans;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.maksimka.questionnaire.data.*;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.util.*;

@Getter
@Setter
@Component
public class FieldAdder {

//    @Autowired
//    private FieldTypeRepo fr;

    @Autowired
    private FieldRepo repo;

    @Autowired
    private CheckBoxOptionsRepo checkboxRepo;

    private Long id = null;

    private String fieldOptions = new String();

    private String type;

    private String label;

    public String[] checkboxOptions; // +getter +setter

    private Boolean isActive = Boolean.FALSE;
    private Boolean isRequired = Boolean.FALSE;


    public void addField() throws Exception{
        convertCheckbox();
        if(id == null){
            Field field = new Field(label, FieldType.valueOf(type), isActive, isRequired);
            repo.save(field);
            parseOptions(field.getId());
            RequestContext.getCurrentInstance().closeDialog("updateField");
        }else {
            Field field = repo.findById(id).get();
            field.setLabel(label);
            field.setType(FieldType.valueOf(type));
            field.setActive(isActive);
            field.setRequired(isRequired);
            checkboxRepo.deleteAllByField(field);
            repo.save(field);
            if (field.getType().equals(FieldType.COMBOBOX) ||
                    field.getType().equals(FieldType.RADIOBUTTON) ||
                    field.getType().equals(FieldType.CHECKBOX)) {
                parseOptions(field.getId());
            }
        }
        refresh();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext() ;
        externalContext.redirect("/fields");
    }

    public void refresh(){

        id = null;
        label = null;
        isActive = null;
        isRequired = null;
        type = null;
        checkboxOptions = new String[2];
        fieldOptions = "";
    }

    private void convertCheckbox(){
        isActive = false;
        isRequired = false;
        for(String s : checkboxOptions){
            switch (s){
                case "required":
                    isRequired = true;
                    break;
                case "active":
                    isActive = true;
                    break;
                default: break;
            }
        }

    }

    private void parseOptions(long field_id){
        StringTokenizer tokenizer = new StringTokenizer(fieldOptions);
        String token;
        while(tokenizer.hasMoreTokens()){
            token = tokenizer.nextToken();
            CheckBoxOption option = new CheckBoxOption();

            option.setField(repo.findById(field_id).get());
            option.setOptionValue(token);
            checkboxRepo.save(option);
        }


    }

    private List<CheckBoxOption>asList(){
        List<CheckBoxOption> options = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(fieldOptions);
        String token;
        while((token = tokenizer.nextToken()) !=null){
            options.add(new CheckBoxOption(token));

        }
        return options;
    }

//    public void add(){
//        fr.save(FieldType.TEXTFIELD);
//        fr.save(FieldType.TEXTAREA);
//        fr.save(FieldType.RADIOBUTTON);
//        fr.save(FieldType.CHECKBOX);
//        fr.save(FieldType.COMBOBOX);
//        fr.save(FieldType.DATE);
//    }

}
