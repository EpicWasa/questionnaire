package ua.maksimka.questionnaire.beans;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.primefaces.context.RequestContext;


import javax.faces.bean.RequestScoped;
import java.util.HashMap;
import java.util.Map;
import javax.faces.event.ActionEvent;

@Component

public class FieldView {

    @Autowired
    private FieldAdder adder;

    public void addField() {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("resizable", false);
        options.put("draggable", true);
        options.put("modal", true);
        options.put("height", 400);
        options.put("contentHeight", "100%");
        adder.refresh();
        RequestContext.getCurrentInstance().openDialog("updateField", options, null);
    }
}
