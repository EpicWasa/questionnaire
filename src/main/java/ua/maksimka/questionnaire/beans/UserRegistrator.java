package ua.maksimka.questionnaire.beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ua.maksimka.questionnaire.data.User;
import ua.maksimka.questionnaire.data.UserRepo;


import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;


@Component
public class UserRegistrator {

    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    UserRepo repo;

    private String firstName;

    private String lastName;

    private String email;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String password;

    private String phone;

    public void addUser() throws Exception{
        User userFromRequest = repo.findByEmail(email);

        if(userFromRequest != null){
            return;
        }
        userFromRequest= new User();
        userFromRequest.setFirstName(firstName);
        userFromRequest.setLastName(lastName);
        userFromRequest.setEmail(email);
        userFromRequest.setPassword(password);
        userFromRequest.setActive(Boolean.TRUE);
        userFromRequest.setPhone(phone);
        repo.save(userFromRequest);
        //emailService.sendSimpleMessage(userFromRequest.getEmail(), "Registration", "Congrats!");
        setEmpty();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext() ;
        externalContext.redirect("login.xhtml");

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void deleteAll(){
        repo.deleteAll();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private void setEmpty(){
        email = null;
        firstName=null;
        lastName=null;
        email=null;
        password = null;
    }
}
