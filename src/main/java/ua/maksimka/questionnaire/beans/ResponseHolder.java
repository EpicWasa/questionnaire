package ua.maksimka.questionnaire.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.maksimka.questionnaire.data.*;

import java.util.HashMap;

@Component
public class ResponseHolder {

//    @Autowired
//    private ResponseRepo responseRepo;
//
//    @Autowired
//    private FieldRepo fieldRepo;



    public String getValue(Response response, Field field){
        String res = "";
        for(ResponseData data : response.getResponseDataSet()){
            if(data.getField().equals(field)){
                res = res.concat( data.getFieldValue()+" ");
            }
        }
        return res.replaceAll(" ","").equals("")? "N/A": res;
    }

}
