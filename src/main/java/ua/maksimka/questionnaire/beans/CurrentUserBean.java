package ua.maksimka.questionnaire.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ua.maksimka.questionnaire.data.User;
import ua.maksimka.questionnaire.data.UserRepo;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

@Getter
@Setter
@Component
public class CurrentUserBean {

    private String newPhone = null;

    private String newFirstName = null;

    private String newLastName = null;

    private String newEmail =null;

    @Autowired
    UserRepo repo;

    public boolean checkWhetherUserLogged(){
        boolean res = (
                SecurityContextHolder.getContext().getAuthentication() != null &&
                        SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&
//when Anonymous Authentication
                        !(SecurityContextHolder.getContext().getAuthentication()
                                instanceof AnonymousAuthenticationToken) );
         return res ;
    }

    private User getCurrentUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return repo.findByEmail(((UserDetails)principal).getUsername());
    }

    public String getEmail(){
        return getCurrentUser().getEmail();
    }

    public String getPhone(){

        return  getCurrentUser().getPhone();
                //phone.replaceAll(" ", "").replaceAll("([(,)])", "").replaceAll("-",""); //ЕЗУМИЕ
    }

    public String getFirstName(){
        return getCurrentUser().getFirstName();
    }

    public String getLastName(){
        return getCurrentUser().getLastName();
    }

    public void updateUser() throws Exception{
        User userFromRequest = repo.findByEmail(newEmail);

        if(!(newEmail.equals(getEmail()) || userFromRequest == null)){
            return;
        }
        userFromRequest= getCurrentUser();
        userFromRequest.setFirstName(newFirstName);
        userFromRequest.setLastName(newLastName);
        userFromRequest.setEmail(newEmail);
        userFromRequest.setPhone(newPhone);
        repo.save(userFromRequest);

        setEmpty();

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext() ;
        externalContext.redirect("/");
    }

    private void setEmpty(){
        newPhone = null;
        newEmail=null;
        newFirstName=null;
        newLastName=null;
    }

    public String getNewFirstName() {
        if (newFirstName == null){
            return getFirstName();
        }
        return newFirstName;
    }

    public void setNewFirstName(String newFirstName) {
        this.newFirstName = newFirstName;
    }

    public String getNewLastName() {
        if (newLastName == null){
            return getLastName();
        }
        return newLastName;
    }

    public void setNewLastName(String newLastName) {
        this.newLastName = newLastName;
    }

    public String getNewEmail() {
        if (newEmail == null){
            return getEmail();
        }
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getUserMenuName(){
        String res = getFirstName()+" "+getLastName();
        return (res == null || res.equals(" "))? "User" : res;

    }

    public void logout() throws Exception{

        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext() ;
        externalContext.redirect("/logout");
    }
}
